import gzip
import json
import pickle
from . import pmp_filters
from flask import Flask, request

global filter
app = Flask(__name__)

write = False
try:
    with gzip.open('filter.gz', 'rb') as f:
        filter = f.read()
    filter = pickle.loads(filter)
except FileNotFoundError:
    print("Creating filter object")
    write = True
    filter = pmp_filters.PmpFilter()

if write:
    with gzip.open('filter.gz', 'wb') as f:
        f.write(pickle.dumps(filter))


@app.route("/", methods=['POST'])
def check_filter():
    deal = json.loads(request.data.decode('utf-8'))
    result = filter.process(deal)
    return json.dumps(result)
